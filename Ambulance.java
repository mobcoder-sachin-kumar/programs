import java.util.Scanner;

public class Ambulance {
    static int n = 0,m = 0,sum1 = 0, sum2 = 0, ambA =0, ambB = 0;
    int ambulanceA(int n,int m, int[][] mat)
    {
        sum1 = mat[0][0];
        for (int i = 1; i < n; i++)
        {
            int max = 0;
            for (int j = 0; j < m/2; j++)
            {
                if(mat[i][j]>max)
                {
                    max = mat[i][j];
                }
            }
            sum1 = sum1 + max;
        }
        return sum1;
    }

    int ambulanceB(int n, int m, int[][] mat)
    {
        sum2 = mat[0][m-1];
        for (int i = 1; i < n; i++)
        {
            int max = 0;
            for (int j = m/2; j <= m-1; j++)
            {
                if(mat[i][j]>max)
                {
                    max = mat[i][j];
                }
            }
            sum2 = sum2 + max;
        }
        return sum2;
    }

    public static void main(String[] args)
    {

        Scanner sc =  new Scanner(System.in);
        Ambulance obj = new Ambulance();
        System.out.println("Enter No. of rows & columns :");
        obj.n = sc.nextInt();
        obj.m = sc.nextInt();
        int mat[][] =  new int[obj.n][obj.m];

        System.out.println("Enter Matrix details :");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
              mat[i][j] = sc.nextInt();
            }
        }

        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < m; j++)
            {
                System.out.print(mat[i][j]);
                System.out.print(" ");
            }
            System.out.print("\n");
        }

        ambA = obj.ambulanceA(n,m, mat);
        System.out.println("Maximum patient carried by ambulance 1 :"+ambA);
        ambB = obj.ambulanceB(n,m,mat);
        System.out.println("Maximum patient carried by ambulance 2 :"+ambB);

        System.out.println("Maximum patient carried by ambulance 1 & 2 :" +(ambA+ambB));

    }
}
