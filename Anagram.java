import java.util.*;

public class Anagram 
{
    public static List<List<String>> pairAnagram(String str[])
    {
        List<List<String>> res = new ArrayList<>();
        HashMap<String, List<String>> map = new HashMap<>();

        for(String str1: str)
        {
            char[] charr= str1.toCharArray();
            Arrays.sort(charr);

            String key = new String(charr);

            if(map.containsKey(key))
            {
                map.get(key).add(str1);
            }
            else
            {
                List<String> strList = new ArrayList<>();
                strList.add(str1);
                map.put(key, strList);
            }
        }
        res.addAll(map.values());
        return res;
    }

    public static void main(String args[])
    {
        
        String str[] = new String[7];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 7; i++) 
        {
            str[i] = sc.next();   
        }

        for (int i = 0; i < 7; i++) 
        {
            System.out.println(str[i]);   
        }
        List<List<String>> res = pairAnagram(str);

        res.forEach(t -> System.out.println(t + " "));
    }
}